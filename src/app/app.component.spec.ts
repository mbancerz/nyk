import { AppModule } from './app.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppComponent } from './app.component';

describe('AppComponent', () => {
    let fixture: ComponentFixture<AppComponent>;
    let app: AppComponent;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [],
            imports: [AppModule]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(AppComponent);
            app = fixture.debugElement.componentInstance;
        });
    }));

    it('should create the app', (() => {
        expect(app).toBeTruthy();
    }));
});
