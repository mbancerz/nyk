import { CustomersApiResponse, Customer } from '../../customers/customer.model';
import { AccountForm } from '../account.model';
import { ApiService } from '../../../shared/api/api.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from '@angular/material';

@Component({
    selector: 'app-account-create',
    templateUrl: 'account-create.component.html'
})
export class AccountCreateComponent implements OnInit {
    public isReady = false;
    public form: FormGroup;
    public customers: Customer[];

    constructor(
        private fb: FormBuilder,
        private api: ApiService,
        private snackBar: MdSnackBar) { }

    ngOnInit() {
        this.createForm();
    }

    private createForm() {
        this.api.getCustomers()
        .then((customers) => this.customers = customers)
        .then(() => {
            this.form = this.fb.group(<AccountForm>{
                cprCvr: new FormControl('', Validators.required),
                regNo: new FormControl('', Validators.required),
                accountNo: new FormControl('', Validators.required),
                name: new FormControl('', Validators.required)
            });
            this.isReady = true;
        });
    }

    public create = () => {
        return this.api.putAccount(this.form.get('regNo').value, this.form.get('accountNo').value, this.form.value)
        .then((response) => {
            if (response) {
                this.snackBar.open(`Account has been created`, 'OK', { duration: 3000 });
            } else {
                this.snackBar.open('Creating failed :(', 'Error', { duration: 3000 });
            }
        });
    }
}
