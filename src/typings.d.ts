/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
    id: string;
}

/** OTHER **/

type TokenResponse = {
    access_token: string;
    token_type: string;
    expires_in: number;
}