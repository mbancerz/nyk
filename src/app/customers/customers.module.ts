import { RouterModule } from '@angular/router';
import { CustomerCreateComponent } from './create/customer-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomerDetailsComponent } from './details/customer-details.component';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../shared/material.module';
import { ApiService } from '../../shared/api/api.service';
import { CustomersComponent } from './customers.component';
import { NgModule } from '@angular/core';
import { customersRoutes } from './customers.routing';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(customersRoutes),
        MaterialModule,
        ReactiveFormsModule
    ],
    exports: [],
    declarations: [
        CustomersComponent,
        CustomerDetailsComponent,
        CustomerCreateComponent
    ],
    providers: [
        ApiService
    ]
})
export class CustomersModule {}
