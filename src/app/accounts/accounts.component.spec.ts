import { MockActivatedRoute, MockRouter } from '../../mocks/mock.router';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { ApiService } from '../../shared/api/api.service';
import { AccountsModule } from './accounts.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsComponent } from './accounts.component';


describe('AccountsComponent', () => {
    let component: AccountsComponent;
    let fixture: ComponentFixture<AccountsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [AccountsModule, HttpModule, RouterModule],
            providers: [
                ApiService,
                { provide: Router, useClass: MockRouter },
                { provide: ActivatedRoute, useClass: MockActivatedRoute }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
