import { FormControl } from '@angular/forms';
export interface ICustomer {
    cprCvr: string;
    firstName: string;
    lastName: string;
    _links: {
        members: string[];
        relations: ILink[];
    };
}

export class Customer implements ICustomer {
    public cprCvr: string;
    public firstName: string;
    public lastName: string;
    public _links: {
        members: string[];
        relations: ILink[];
    };

    public get links() {
        return this._links;
    }

    constructor(data?: ICustomer) {
        Object.assign(this, data);
    }

    public static factory(data: ICustomer): Customer {
        return new Customer(data);
    }
}

export type CustomerForm = {
    [k in keyof ICustomer]?: FormControl
};

export interface CustomersApiResponse {
    _embedded: {
        customers: Customer[];
    };
    _links: {
        self: ILink
    };
}

interface ILink {
    href?: string;
    name?: string;
}

export interface CustomerApiResponse extends ICustomer {}
