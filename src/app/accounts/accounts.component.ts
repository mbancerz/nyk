import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { ApiService } from '../../shared/api/api.service';
import { Account, IAccount } from './account.model';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-accounts',
    templateUrl: './accounts.component.html',
    styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {

    public list: Account[];

    constructor(private api: ApiService, private router: Router) { }

    ngOnInit() {
        this.getAll();
    }

    private getAll = () => {
        this.api.getAccounts().then((accounts) => this.list = accounts);
    }

    public details(account: IAccount) {
        this.router.navigate(['accounts', account.regNo, account.accountNo]);
    }

}
