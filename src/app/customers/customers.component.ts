import { Router } from '@angular/router';
import { Customer, ICustomer } from './customer.model';
import { ApiService } from '../../shared/api/api.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-customers',
    templateUrl: './customers.component.html',
    styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

    public list: Customer[];

    constructor(private api: ApiService, private router: Router) { }

    ngOnInit() {
        this.getAll();
    }

    private getAll = () => {
        this.api.getCustomers().then((customers) => this.list = customers);
    }

    public details(customer: ICustomer) {
        this.router.navigate(['customers', customer.cprCvr]);
    }

}
