import { NgModule } from '@angular/core';
import {
    MdButtonModule,
    MdTabsModule,
    MdListModule,
    MdIconModule,
    MdFormFieldModule,
    MdInputModule,
    MdToolbarModule,
    MdProgressBarModule,
    MdSnackBarModule,
    MdAutocompleteModule
} from '@angular/material';

const components = [
    MdButtonModule,
    MdTabsModule,
    MdListModule,
    MdIconModule,
    MdFormFieldModule,
    MdInputModule,
    MdToolbarModule,
    MdProgressBarModule,
    MdSnackBarModule,
    MdAutocompleteModule
];

@NgModule({
    imports: [...components],
    exports: [...components],
})
export class MaterialModule { }
