import { ApiService } from '../shared/api/api.service';
import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [ApiService]
})
export class AppComponent {

    public tabs = [
        { path: 'accounts', label: 'Accounts' },
        { path: 'customers', label: 'Customers' }
    ];

    constructor(apiService: ApiService) {
        apiService.getToken().then((token) => {
            console.log('Token: ', token);
        });
    }
}
