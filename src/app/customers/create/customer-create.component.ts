import { CustomerForm } from '../customer.model';
import { ApiService } from '../../../shared/api/api.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from '@angular/material';

@Component({
    selector: 'app-customer-create',
    templateUrl: 'customer-create.component.html'
})
export class CustomerCreateComponent implements OnInit {
    public form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private api: ApiService,
        private snackBar: MdSnackBar) { }

    ngOnInit() {
        this.createForm();
    }

    private createForm() {
        this.form = this.fb.group(<CustomerForm>{
            cprCvr: new FormControl('', Validators.required),
            firstName: new FormControl('', Validators.required),
            lastName: new FormControl('', Validators.required)
        });
    }

    public create = () => {
        return this.api.putCustomer(this.form.get('cprCvr').value, this.form.value).then((response) => {
            if (response) {
                this.snackBar.open(`Customer has been created`, 'OK', { duration: 3000 });
            } else {
                this.snackBar.open('Update failed :(', 'Error', { duration: 3000 });
            }
        });
    }
}