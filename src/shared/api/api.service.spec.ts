import { Account } from '../../app/accounts/account.model';
import { fakeAccountsResponse, fakeAccountResponse } from '../../mocks/mock.accounts';
import { HttpModule } from '@angular/http';
import { ApiService } from './api.service';
import { async, getTestBed, TestBed } from '@angular/core/testing';
describe('API Service', () => {
    let service: ApiService;

    beforeEach(async() => {
        TestBed.configureTestingModule({
            imports: [
                HttpModule
            ],
            providers: [
                ApiService
            ]
        });
    });

    beforeEach(() => {
        service = getTestBed().get(ApiService);
    });

    it('Should create base64 basic auth', () => {
        const authBasic = Reflect.get(service, 'AUTH_BASIC');
        expect(authBasic).toBe('dGVzdC1jbGllbnRpZDpwYXNzd29yZA==');
    });

    it('Should map response to accounts', (done) => {
        const spyOnRequest = spyOn(service, <any>'request').and.returnValue(Promise.resolve(fakeAccountsResponse));
        const expectedResult = fakeAccountsResponse._embedded.accounts.map((acc) => Account.factory(acc));
        const expectedPath = 'cem-hackathon-service/accounts';
        service.getAccounts().then((result) => {
            expect(spyOnRequest).toHaveBeenCalledWith(expectedPath);
            expect(result).toEqual(expectedResult);
            done();
        });
    });

    it('Should return account', (done) => {
        const spyOnRequest = spyOn(service, <any>'request').and.returnValue(Promise.resolve(fakeAccountResponse));
        const expectedResult = Account.factory(fakeAccountResponse);
        const expectedPath = 'cem-hackathon-service/accounts/2-2';
        service.getAccount('2', '2').then((result) => {
            expect(spyOnRequest).toHaveBeenCalledWith(expectedPath);
            expect(result).toEqual(expectedResult);
            done();
        });
    });
});

