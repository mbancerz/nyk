import { Transaction } from './account-transactions.model';
import { Component, Input, OnInit } from '@angular/core';
@Component({
    selector: 'app-account-transactions',
    templateUrl: 'account-transactions.component.html'
})
export class AccountTransactionsComponent implements OnInit {

    @Input()
    public transactions: Transaction[];

    constructor () {

    }
    ngOnInit() {

    }
}