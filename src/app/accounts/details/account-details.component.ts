import { Transaction } from '../transactions/account-transactions.model';
import { Customer, CustomerApiResponse, ICustomer } from '../../customers/customer.model';
import { Validator } from 'codelyzer/walkerFactory/walkerFn';
import { ActivatedRoute } from '@angular/router';
import { Account, AccountApiResponse, AccountForm, IAccount } from '../account.model';
import { ApiService } from '../../../shared/api/api.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from '@angular/material';

@Component({
    selector: 'app-account-details',
    templateUrl: 'account-details.component.html'
})
export class AccountDetailsComponent implements OnInit {

    private regNo: string;
    private accountNo: string;
    public isReady = false;
    public form: FormGroup;
    public account: Account;
    public customer: Customer;
    public transactions: Transaction[];

    constructor(
        private fb: FormBuilder,
        private api: ApiService,
        private activatedRoute: ActivatedRoute,
        private snackBar: MdSnackBar) { }

    private createForm() {
        this.form = this.fb.group(<AccountForm>{
            cprCvr: new FormControl('', Validators.required),
            regNo: new FormControl('', Validators.required),
            accountNo: new FormControl('', Validators.required),
            name: new FormControl('')
        });
    }

    private fillForm() {
        if (this.regNo === '' || this.accountNo === '') {
            this.snackBar.open('Invalid account parameters', 'Error', { duration: 3000 });
            return false;
        }
        return this.api.getAccount(this.regNo, this.accountNo).then((account) => {
            this.account = account;
            this.transactions = this.account.transactions;
        }).then(() => {
            this.api.getCustomerFromHref(this.account.links.customer.href).then((customer) => this.customer = customer)
            .then(() => {
                this.form.setValue({
                    cprCvr: this.customer.cprCvr,
                    regNo: this.account.regNo,
                    accountNo: this.account.accountNo,
                    name: this.account.name
                });
                setTimeout(() => { /* to simulate lenghty requests and display progressbar */
                    this.isReady = true;
                }, 1000);
            });
        }).catch((error) => {
            this.snackBar.open(`Server error: ${error}`, 'Error', { duration: 3000 });
        });
    }

    ngOnInit() {
        this.createForm();

        this.activatedRoute.params.subscribe((params) => {
            this.regNo = params['regNo'] || '';
            this.accountNo = params['accountNo'] || '';
            this.fillForm();
        });
    }

    public update = () => {
        return this.api.putAccount(this.regNo, this.accountNo, this.form.value).then((response) => {
            if (response) {
                this.snackBar.open(`Account ${this.regNo}/${this.accountNo} updated successfuly`, 'OK', { duration: 3000 });
            } else {
                this.snackBar.open('Updating account failed :(', 'Error', { duration: 3000 });
            }
        });
    }
}
