import { FormControl } from '@angular/forms';
import { ITransaction, Transaction } from './transactions/account-transactions.model';
export interface IAccount {
    regNo: string;
    accountNo: string;
    name: string;
    _embedded?: {
        transactions: ITransaction[]
    };
    _links?: {
        'account:transactions': ILink,
        customer: ILink,
        self: ILink
    };
}

export class Account implements IAccount {
    public regNo: string;
    public accountNo: string;
    public name: string;
    public _embedded: {
        transactions: ITransaction[]
    };
    public _links: {
        'account:transactions': ILink,
        customer: ILink,
        self: ILink
    };

    public get transactions() {
        return this._embedded.transactions.map((transaction) => Transaction.factory(transaction));
    }

    public get links() {
        return this._links;
    }

    constructor(data?: IAccount) {
        Object.assign(this, data);
    }

    public static factory(data: IAccount): Account {
        return new Account(data);
    }
}

export interface AccountsApiResponse {
    _embedded?: {
        accounts: IAccount[]
    };
    _links?: any;
}

interface ILink {
    href?: string;
}

export type AccountForm = {
    [k in keyof IAccount]?: FormControl
};

export interface AccountApiResponse {
    regNo: string;
    accountNo: string;
    name: string;
    _embedded?: {
        transactions: ITransaction[]
    };
    _links?: {
        'account:transactions': ILink,
        customer: ILink,
        self: ILink
    };
}
