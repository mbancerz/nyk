import { AccountCreateComponent } from './create/account-create.component';
import { AccountDetailsComponent } from './details/account-details.component';
import { AccountsComponent } from './accounts.component';
import { RouterModule, Routes } from '@angular/router';
export const accountsRoutes: Routes = [
    {
        path: '',
        component: AccountsComponent,
        pathMatch: 'full'
    },
    {
        path: 'accounts/create',
        component: AccountCreateComponent
    },
    {
        path: 'accounts/:regNo/:accountNo',
        component: AccountDetailsComponent
    }
];
