export interface ITransaction {
    id: string;
    description: string;
    amount: string;
}

export class Transaction implements ITransaction {
    public id: string;
    public description: string;
    public amount: string;

    constructor(data?: ITransaction) {
        Object.assign(this, data);
    }

    public static factory(data: ITransaction): Transaction {
        return new Transaction(data);
    }
}
