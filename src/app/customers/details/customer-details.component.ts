import { Customer, CustomerForm, ICustomer } from '../../customers/customer.model';
import { Validator } from 'codelyzer/walkerFactory/walkerFn';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../shared/api/api.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from '@angular/material';

@Component({
    selector: 'app-customer-details',
    templateUrl: 'customer-details.component.html'
})
export class CustomerDetailsComponent implements OnInit {

    private cprCvr: string;
    public isReady = false;
    public form: FormGroup;
    public customer: Customer;

    constructor(
        private fb: FormBuilder,
        private api: ApiService,
        private activatedRoute: ActivatedRoute,
        private snackBar: MdSnackBar) { }

    private createForm() {
        this.form = this.fb.group(<CustomerForm>{
            cprCvr: new FormControl('', Validators.required),
            firstName: new FormControl('', Validators.required),
            lastName: new FormControl('', Validators.required)
        });
    }

    private fillForm() {
        if (this.cprCvr === '' ) {
            this.snackBar.open('Invalid customer parameters', 'Error', { duration: 3000 });
            return false;
        }
        return this.api.getCustomer(this.cprCvr)
        .then((customer) => this.customer = customer)
        .then(() => {
            this.form.setValue(<ICustomer>{
                cprCvr: this.customer.cprCvr,
                firstName: this.customer.firstName || '',
                lastName: this.customer.lastName || ''
            });
            setTimeout(() => { /* to simulate lenghty requests and display progressbar */
                this.isReady = true;
            }, 1000);
        }).catch((error) => {
            this.snackBar.open(`Server error: ${error}`, 'Error', { duration: 3000 });
        });
    }

    ngOnInit() {
        this.createForm();

        this.activatedRoute.params.subscribe((params) => {
            console.log({ params });
            this.cprCvr = params['cprCvr'] || '';
            this.fillForm();
        });
    }

    public update = () => {
        return this.api.putCustomer(this.cprCvr, this.form.value).then((result) => {
            if (result) {
                this.snackBar.open(`Customer ${this.cprCvr} updated successfuly`, 'OK', { duration: 3000 });
            } else {
                this.snackBar.open('Update failed :(', 'Error', { duration: 3000 });
            }
        });
    }
}
