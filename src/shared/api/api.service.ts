import { AccountApiResponse, AccountsApiResponse, Account } from '../../app/accounts/account.model';
import { Customer, CustomerApiResponse, CustomersApiResponse } from '../../app/customers/customer.model';
import { Headers, Http, RequestOptionsArgs } from '@angular/http';
import { Injectable } from '@angular/core';
@Injectable()
export class ApiService {

    private HOST = 'https://i1.test-services.nykredit.it';
    private CLIENT_ID = 'test-clientid';
    private CLIENT_SECRET = 'password';
    private AUTH_BASIC = '';

    constructor(private http: Http) {
        this.AUTH_BASIC = btoa(this.CLIENT_ID + ':' + this.CLIENT_SECRET);
    }

    private request = <T>(path: string, fullPath = false) => {
        const headers = new Headers();
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('nykredit-token'));
        headers.append('Accept', 'application/hal+json');
        headers.append('X-Client-Version', '1.0.0');
        const opts: RequestOptionsArgs = {
            headers: headers
        };
        return new Promise<T>((onSuccess, onError) => {
            const url = fullPath ? path : this.HOST + '/' + path;
            this.http.get(url, opts).subscribe((response) => {
                onSuccess(response.json());
             }, (error) => {
                onError(error);
             });
        });
    }

    private send = (path: string, body: {[index: string]: any}) => {
        const headers = new Headers();
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('nykredit-token'));
        headers.append('Accept', 'application/hal+json');
        headers.append('X-Client-Version', '1.0.0');
        const opts: RequestOptionsArgs = {
            headers: headers
        };
        return new Promise<boolean>((onSuccess, onError) => {
            this.http.put(this.HOST + '/' + path, body, opts).subscribe((response) => {
                onSuccess(response.status === 200);
             }, (error) => {
                onError(error);
             });
        });
    }

    private haveToken = () => localStorage.getItem('nykredit-token') !== '';

    public getToken = () => {
        const body = 'grant_type=client_credentials';
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + this.AUTH_BASIC);
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const opts: RequestOptionsArgs = {
            headers: headers
        };
        return new Promise<string>((onSuccess, onError) => {
            this.http.post(this.HOST + '/security/oauth2/token', body, opts).subscribe((response) => {
                const result: TokenResponse = response.json();
                localStorage.setItem('nykredit-token', result.access_token);
                onSuccess(result.access_token);
             }, (error) => {
                onError(error);
             });
        });
    }

    public getCustomers = (): Promise<Customer[]> => {
        const path = 'cem-hackathon-service/customers';
        return this.request<CustomersApiResponse>(path).then((response) => {
            return response._embedded.customers.map(data => Customer.factory(data));
        });
    }

    public getCustomer = (cprCvr: string): Promise<Customer> => {
        const path = `cem-hackathon-service/customers/${cprCvr}`;
        return this.request<CustomerApiResponse>(path).then((response) => {
            return Customer.factory(response);
        });
    }

    public getCustomerFromHref = (href: string): Promise<Customer> => {
        return this.request<CustomerApiResponse>(href, true).then((response) => {
            return Customer.factory(response);
        });
    }

    public putCustomer = (cprCvr: string, body: any) => {
        const path = `cem-hackathon-service/customers/${cprCvr}`;
        return this.send(path, body);
    }

    public getAccounts = (): Promise<Account[]> => {
        const path = 'cem-hackathon-service/accounts';
        return this.request<AccountsApiResponse>(path).then((response) => {
            return response._embedded.accounts.map(data => Account.factory(data));
        });
    }

    public getAccount = (regNo: string, accountNo: string): Promise<Account> => {
        const path = `cem-hackathon-service/accounts/${regNo}-${accountNo}`;
        return this.request<AccountApiResponse>(path).then((response) => Account.factory(response));
    }

    public putAccount = (regNo: string, accountNo: string, body: any) => {
        const path = `cem-hackathon-service/accounts/${regNo}-${accountNo}`;
        return this.send(path, body);
    }

}