import { MockActivatedRoute, MockRouter } from '../../mocks/mock.router';
import { ApiService } from '../../shared/api/api.service';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { CustomersModule } from './customers.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersComponent } from './customers.component';

describe('CustomersComponent', () => {
    let component: CustomersComponent;
    let fixture: ComponentFixture<CustomersComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [],
            imports: [CustomersModule, HttpModule, RouterModule],
            providers: [
                ApiService,
                { provide: Router, useClass: MockRouter },
                { provide: ActivatedRoute, useClass: MockActivatedRoute }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CustomersComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
