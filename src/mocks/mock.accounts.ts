import { AccountApiResponse, AccountsApiResponse, IAccount } from '../app/accounts/account.model';
export const fakeAccountsResponse: AccountsApiResponse = {
    _embedded: {
        accounts: [{
            accountNo: '1',
            regNo: '1',
            name: 'A'
        }]
    },
    _links: {}
};

export const fakeAccountResponse: AccountApiResponse = {
    accountNo: '2',
    regNo: '2',
    name: 'B',
    _embedded: {
        transactions: []
    }
};
