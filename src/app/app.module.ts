import { HttpModule } from '@angular/http';
import { MaterialModule } from '../shared/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CustomersModule } from './customers/customers.module';
import { AccountsModule } from './accounts/accounts.module';

import { RouterModule } from '@angular/router';
import { AppRoutes } from './app.routing';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule,
        CustomersModule,
        AccountsModule,
        HttpModule,
        RouterModule.forRoot(AppRoutes, {
            useHash: true,
            enableTracing: false
        })
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
