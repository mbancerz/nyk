import { AccountTransactionsComponent } from './transactions/account-transactions.component';
import { AccountCreateComponent } from './create/account-create.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountDetailsComponent } from './details/account-details.component';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../shared/material.module';
import { AccountsComponent } from './accounts.component';
import { NgModule } from '@angular/core';
import { accountsRoutes } from './accounts.routing';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(accountsRoutes),
        MaterialModule,
        ReactiveFormsModule
    ],
    exports: [],
    declarations: [
        AccountsComponent,
        AccountDetailsComponent,
        AccountCreateComponent,
        AccountTransactionsComponent
    ]
})
export class AccountsModule {}
