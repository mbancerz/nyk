import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
export const AppRoutes: Routes = [
    {
        path: '',
        component: AppComponent
    },
    {
        path: 'accounts',
        loadChildren: './accounts/accounts.module#AccountsModule'
    },
    {
        path: 'customers',
        loadChildren: './customers/customers.module#CustomersModule'
    }
];