import { CustomerCreateComponent } from './create/customer-create.component';
import { CustomerDetailsComponent } from './details/customer-details.component';
import { CustomersComponent } from './customers.component';
import { RouterModule, Routes } from '@angular/router';
export const customersRoutes: Routes = [
    {
        path: '',
        component: CustomersComponent
    },
    {
        path: 'customers/create',
        component: CustomerCreateComponent
    },
    {
        path: 'customers/:cprCvr',
        component: CustomerDetailsComponent
    }
];
